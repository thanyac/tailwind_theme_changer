module.exports = {
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
    require('@tailwindcss/ui'),
  ],
  theme: {
    extend: {
      colors: {
        dark: {
          100: '#6d706f',
          200: '#20c991',
          300: '#009a9a',
          400: '#ffffff',
          500: '#000000',
        },
        light: {
          100: '#EAE4E9',
          200: '#DFE7FD',
          300: '#FDE2E4',
          400: '#f4737b',
          500: '#E2ECE9',
        },
        light2: {
          100: '#EDF6F9',
          200: '#FFDDD2',
          300: '#E29578',
          400: '#006D77',
          500: '#83C5BE',
        },
      },
    },
  },
};
